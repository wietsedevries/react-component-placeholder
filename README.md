# React component placeholder

A toolbox to create highly customisable responsive component placeholders.

![img](http://wietsedevries.eu/rcp/placeholder.gif)

It's easy to use and very flexible. Feel free to use these components in combination with your own custom components or components from other libraries. No restrictions.


## Index

1. [Setup](#setup)
2. [Components](#components)
    * [Placeholder](#placeholder)
    * [Block](#block)
    * [Circle](#circle)
    * [Square](#square)
    * [Text](#text)
    * [Line](#line)
3. [Grid](#grid)
    * [Row](#row)
    * [Col](#col)
4. [Contribute](#contribute)
5. [Change log](#log)
6. [License](#license)



## <a name="setup"></a>Setup

Install react-component-placeholder as a dependency for your react.js project.

```bash
npm install react-component-placeholder --save
```

## <a name="components"></a>Components

This library consist of a few simple react components you can use to compose your own placeholders.

**Example**

```javascript
import React from 'react';
import { Placeholder, Circle, Text, Row, Col } from 'react-component-placeholder';

export const customPlaceholderComponent = () => {
  const config = {
    borderRadius: 3,
    color: '#F9F9F9',
    // etc.
  }

  return (
    <Placeholder config={config}>
      <Row>
        <Col xs={4} md={3}>
          <Circle />
        </Col>
        <Col xs={4} md={3}>
          <Text />
        </Col>
      </Row>
    </Placeholder>
  )
}
```

 

### <a name="placeholder"></a>Placeholder

The Placeholder component is a provider component to provide nested components with context. You can pass a single config prop to the Placeholder component.

```javascript
const config = {
  animation: 'pulse',
  borderRadius: 5,
  color: '#CCC',
  fontSize: 16,
}

<Placeholder config={config} />
```
| Property | Description | Type | Default value |
| ------------ | ------------- | ------------- | ------------- |
| animation | Loading animation | ['wave', 'pulse'] | none |
| borderRadius | Border-radius for all children | number | 0 |
| color | Main color for all children | number | 0 |
| viewport | Breakpoints for the [grid components](#grid) | object | `{ xs: 360, sm: 768, md: 1024, lg: 1280 }` |
| columns | Number of rows in the grid | number | 12 |
| gutter | Pixels between rows | number | 30 |

 

### <a name="block"></a>Block

The block component is a simple container you can use to style your placeholder component. All props are optional, but if you don't use them, you might as wel not use the Block component. All props overwrite the config.

```html
<Block borderRadius={2} boxShadow={true} />
```
| Property | Description | Type | Default value |
| ------------ | ------------- | ------------- | ------------- |
| height | Fixed height | number | none |
| padding | Component padding  | number | 0 |
| borderRadius | Border radius for block | number | ```config.borderRadius or 0``` |
| backgroundColor | Background color for block | string | #FFF |
| boxShadow | Box shadow for block. If true default will apply, else the provided value | [string, boolean] | ```'0 1px 1px 0 rgba(60,64,67,.08), 0 1px 3px 1px rgba(60,64,67,.16)' or none ``` |
| border | Border for block. If true default will apply, else the provided value as width | [string, boolean] | ```'1px solid config.color' or none ``` |
| style | Style prop to customise your block | object | none |

 

### <a name="circle"></a>Circle

Simple circle component as a placeholder for round images/buttons.

```html
<Circle backgroundImage="../assets/images/dummy-avatar.png" />
```

| Property | Description | Type | Default value |
| ------------ | ------------- | ------------- | ------------- |
| backgroundColor | Background color | string | ```config.color``` |
| backgroundImage | Background image | string | none |

 

### <a name="square"></a>Square

Simple square component as a placeholder for images.

```html
<Square backgroundColor="#336699" />
```

| Property | Description | Type | Default value |
| ------------ | ------------- | ------------- | ------------- |
| backgroundColor | Background color | string | ```config.color``` |
| backgroundImage | Background image | string | none |
| borderRadius | Border radius for square component | number | ```config.borderRadius``` |

 

### <a name="text"></a>Text

A text placeholder for, well... text.

```html
<Text rows={3} fontSize={11} />
```

| Property | Description | Type | Default value |
| ------------ | ------------- | ------------- | ------------- |
| rows | Number of rows | number | 1 |
| width | Fixed width in px. Only applicable for 1 row | numer | none |
| color | Color of text | string | ```config.color``` |
| fontSize | Font size in pixels | number | 16 |
| borderRadius | Border radius of text line | number | ```(fontSize / 2) or 0 ``` |

 

### <a name="line"></a>Line

A simple horizontal line to use as a divider.

```html
<Line width={2} />
```

| Property | Description | Type | Default value |
| ------------ | ------------- | ------------- | ------------- |
| width | Line width | number | 1 |
| color | Color of the line | string | ```config.color``` |
| margin | Top- and bottom margin for line | number | 10 |

 

## <a name="grid"></a>Grid

This library contains simple grid component, but you're free to use whatever grid components you're comfortable with.

### <a name="row"></a>Row

A horizontal row to contain ```<Col />``` components

```html
<Row />
```

:grey_exclamation: This component does not accept any props.

 

### <a name="col"></a>Col

A column to use inside a ```<Row />```. The width of each column is a percentage determined by the number of horizontal columns defined in the config. 
```(width = (100 / config.columns) * value)```.
The same goes for offset.


```html
<Col xs={12} sm={6} smOffset={3} md={4} lg={3} xl={2} />
```

| Property | Description | Type | Default value |
| ------------ | ------------- | ------------- | ------------- |
| xs | Width for xs viewport | number | 12 |
| sm | Width for sm viewport | number | 12 |
| md | Width for md viewport | number | 12 |
| lg | Width for lg viewport | number | 12 |
| xl | Width for xl viewport | number | 12 |
| xsOffset | Offset left for xs viewport | number | 0 |
| smOffset | Offset left for sm viewport | number | 0 |
| mdOffset | Offset left for md viewport | number | 0 |
| lgOffset | Offset left for lg viewport | number | 0 |
| xlOffset | Offset left for xl viewport | number | 0 |


 

## <a name="contribute"></a>Contribute

Feel free to submit a PR! I'm sure there's room for lots of improvements, but you can start with;

*  New loading animations
*  Extended customisation options for components
*  Whatever you want...

When submitting a PR, please add the correct prop-types, respect the eslint rules and update the change log.

 

## <a name="log"></a>Change log

| Date (dd-mm-yyy) | Description | Name | Version |
| ------------ | ------------- | ------------- | ------------- |
| 31-10-2018 | Initial release | [Wietse de Vries](https://gitlab.com/wietsedevries) | 1.0.0 |

 

## <a name="license"></a>License

Licensed under the MIT License, Copyright © 2018-present
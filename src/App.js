import React, { Component, Fragment } from 'react';
import { Placeholder, Row, Col, Text, Block } from './react-component-placeholder';

import img from './assets/img.jpg';
import './assets/styles.css';

/**
 * Dummy component for demo purpose
 */
const RealComponent = () => {
  return (
    <div className="component">
      <img src={img} width="100%" height="150" />
      <div>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
      </div>
    </div>
  );
};

/**
 * Composed placeholder component
 */
const PlaceholderComponent = () => {
  return (
    <Block boxShadow>
      <Block
        backgroundColor="#E9EBEE"
        height={150}
        style={{
          borderBottomLeftRadius: 0,
          borderBottomRightRadius: 0,
        }}
      />
      <Block padding="12px">
        <Text rows={3} />
      </Block>
    </Block>
  );
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ loading: false });
    }, 1200);
  }

  get config() {
    return {
      viewport: {
        xs: 360,
        sm: 768,
        md: 1024,
        lg: 1280,
      },
      gutter: 30,
      columns: 12,
      backgroundColor: '#FFF',
      fontSize: 16,
      borderRadius: 5,
      animation: 'pulse',
    };
  }

  render() {
    return (
      <div className="container">
        {this.state.loading ? (
          <Placeholder config={this.config}>
            <Row>
              <Col xs={4}>
                <PlaceholderComponent />
              </Col>
              <Col xs={4}>
                <PlaceholderComponent />
              </Col>
              <Col xs={4}>
                <PlaceholderComponent />
              </Col>
            </Row>
          </Placeholder>
        ) : (
          <Fragment>
            <div className="row">
              <div className="column">
                <RealComponent />
              </div>
              <div className="column">
                <RealComponent />
              </div>
              <div className="column">
                <RealComponent />
              </div>
            </div>
          </Fragment>
        )}
      </div>
    );
  }
}

export default App;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ctx } from '../utils';

class Block extends Component {
  getValue(value, fallBack) {
    if (!value) {
      return;
    }
    return typeof(value) === typeof(true) ? fallBack : value;
  }

  get gutter() {
    const { gutter } = this.context;
    return gutter ? (gutter / 2) :  15;
  }

  get style() {
    const { borderRadius: contextBorderRadius, color } = this.context;
    const { borderRadius: propsBorderRadius, backgroundColor, padding, height, border, boxShadow } = this.props;
    const borderRadius = propsBorderRadius || contextBorderRadius;
    return {
      width: '100%',
      height: height ? `${height}px` : 'auto',
      backgroundColor: backgroundColor || '#FFF',
      padding: this.getValue(padding, this.gutter),
      boxSizing: 'border-box',
      borderRadius: `${borderRadius}px`,
      border: this.getValue(border, `1px solid ${color}`),
      boxShadow: this.getValue(boxShadow, '0 1px 1px 0 rgba(60,64,67,.08), 0 1px 3px 1px rgba(60,64,67,.16)'),
    };
  }

  render() {
    return <div name="block" style={{ ...this.style, ...this.props.style }}>{this.props.children}</div>;
  }
}

Block.contextType = ctx;
Block.propTypes = {
  style: PropTypes.object,
  height: PropTypes.number,
  border: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  padding: PropTypes.number,
  children: PropTypes.node,
  boxShadow: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]),
  borderRadius: PropTypes.number,
  backgroundColor: PropTypes.string,
  
};

export default Block;
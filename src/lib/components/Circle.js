import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ctx } from '../utils';

class Circle extends Component {
  get style() {
    const { color } = this.context;
    const { backgroundImage, backgroundColor } = this.props;
    const main = {
      backgroundColor: backgroundColor || color,
      width: '100%',
      paddingTop: '100%',
      borderRadius: '50%',
    };
    const background = backgroundImage ? {
      backgroundImage: backgroundImage ? `url(${backgroundImage})` : 'none',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    } : {};

    return { ...main, ...background };
  }

  render() {
    console.log(typeof(this.props.backgroundImage))
    return (
      <div name="circle" style={this.style} />
    );
  }
}

Circle.contextType = ctx;
Circle.propTypes = {
  backgroundImage: PropTypes.string,
  backgroundColor: PropTypes.string,
};

export default Circle;
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ctx } from '../utils';

class Col extends Component {
  get gutter() {
    const { gutter } = this.context;
    return gutter ? (gutter / 2) :  15;
  }

  get width() {
    const { xs, sm, md, lg, xl } = this.props;
    const { viewport, columns } = this.context;
    const nColumns = columns || 12;
    const dxs = xs || nColumns;
    const grid = {
      xs: (100 / nColumns) * dxs,
      sm: (100 / nColumns) * (sm ? sm : dxs),
      md: (100 / nColumns) * (md ? md : sm ? sm : dxs),
      lg: (100 / nColumns) * (lg ? lg : md ? md : sm ? sm : dxs),
      xl: (100 / nColumns) * (xl ? xl : lg ? lg : md ? md : sm ? sm : dxs),
    };
    return grid[viewport] || 100;
  }

  get offset() {
    const { xsOffset, smOffset, mdOffset, lgOffset, xlOffset } = this.props;
    const { viewport, columns } = this.context;
    const nColumns = columns || 12;
    const grid = {
      xs: xsOffset && (100 / nColumns) * xsOffset,
      sm: smOffset && (100 / nColumns) * smOffset,
      md: mdOffset && (100 / nColumns) * mdOffset,
      lg: lgOffset && (100 / nColumns) * lgOffset,
      xl: xlOffset && (100 / nColumns) * xlOffset,
    };
    return grid[viewport] || 0;
  }

  get style() {
    return {
      width: '100%',
      minHeight: '1px',
      position: 'relative',
      boxSizing: 'border-box',
      flex: `0 0 ${this.width}%`,
      maxWidth: `${this.width}%`,
      marginLeft: `${this.offset}%`,
      paddingLeft: `${this.gutter}px`,
      paddingRight: `${this.gutter}px`,
    };
  }

  render() {
    return (
      <div name="col" style={this.style}>{this.props.children}</div>
    );
  }
}
Col.contextType = ctx;
Col.propTypes = {
  xs: PropTypes.number,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
  xl: PropTypes.number,
  xsOffset: PropTypes.number,
  smOffset: PropTypes.number,
  mdOffset: PropTypes.number,
  lgOffset: PropTypes.number,
  xlOffset: PropTypes.number,
  viewport: PropTypes.string,
  children: PropTypes.node,
};
export default Col;
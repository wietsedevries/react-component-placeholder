import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ctx } from '../utils';

class Line extends Component {
  get style() {
    const { color: contextColor } = this.context;
    const { color: propsColor, width, margin = 10 } = this.props;
    return {
      width: '100%',
      height: width ? `${width}px` : '1px',
      backgroundColor: propsColor || contextColor,
      margin: `${margin}px 0`,
    };
  }

  render() {
    return <div name="line" style={this.style} />;
  }
}

Line.contextType = ctx;
Line.propTypes = {
  width: PropTypes.number,
  color: PropTypes.string,
  margin: PropTypes.number,
};

export default Line;
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ctx } from '../utils';
import '../styles.css';

class Placeholder extends Component {
  constructor(props) {
    super(props);
    const { columns, gutter, color, borderRadius, delay, animation } = this.config;
    this.state = {
      borderRadius: borderRadius || 0,
      color: color || '#E9EBEE',
      animation: animation || '',
      viewport: this.viewport,
      columns: columns || 12,
      gutter: gutter || 30,
    };

    this.getViewport = this.getViewport.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.getViewport);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.getViewport);
  }

  getViewport() {
    this.setState({ viewport: this.viewport });
  }

  get config() {
    return this.props.config || {};
  }

  get viewport() {
    const { viewport } = this.config;
    const width = window.innerWidth;
    const xsMax = viewport ? viewport.xs : 360;
    const smMax = viewport ? viewport.sm : 768;
    const mdMax = viewport ? viewport.md : 1024;
    const lgMax = viewport ? viewport.lg : 1280;
    return (
      width > lgMax ? 'xl' :
        width > mdMax ? 'lg' :
          width > smMax ? 'md' :
            width > xsMax ? 'sm' :
              'xs'
    );
  }

  get style() {
    const { backgroundColor } = this.config;
    return {
      width: '100%',
      backgroundColor: backgroundColor || '#FFF',
    };
  }

  render() {
    const { children } = this.props;
    const { animation } = this.state;

    return (
      <ctx.Provider value={this.state}>
        <div className={animation ? `rcp-${animation}` : ''} name="placeholder" style={this.style}>{children}</div>
      </ctx.Provider>
    );
  }
}

Placeholder.propTypes = {
  config: PropTypes.object,
  children: PropTypes.node,
};

export default Placeholder;
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ctx } from '../utils';

class Row extends Component {
  get gutter() {
    const { gutter } = this.context;
    return gutter ? (gutter / 2) :  15;
  }

  get style() {
    return {
      display: 'flex',
      flexWrap: 'wrap',
      marginLeft: `-${this.gutter}px`,
      marginRight: `-${this.gutter}px`,
    };
  }

  render() {
    return (
      <div name="row" style={this.style}>{this.props.children}</div>
    );
  }
}
Row.contextType = ctx;
Row.propTypes = {
  children: PropTypes.node,
};

export default Row;
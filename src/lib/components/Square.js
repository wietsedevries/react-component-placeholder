import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ctx } from '../utils';

class Square extends Component {
  get style() {
    const { color, borderRadius: contextBorderRadius } = this.context;
    const { backgroundImage, backgroundColor, borderRadius: propsBorderRadius } = this.props;
    const borderRadius = propsBorderRadius || contextBorderRadius;
    const main = {
      backgroundColor: backgroundColor || color,
      width: '100%',
      paddingTop: '100%',
      borderRadius: borderRadius ? `${borderRadius}px` : 0,
    };
    const background = backgroundImage ? {
      backgroundImage: backgroundImage ? `url(${backgroundImage})` : 'none',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    } : {};

    return { ...main, ...background };
  }

  render() {
    return (
      <div name="square" style={this.style} />
    );
  }
}

Square.contextType = ctx;
Square.propTypes = {
  backgroundImage: PropTypes.string,
  backgroundColor: PropTypes.string,
  borderRadius: PropTypes.number,
};

export default Square;
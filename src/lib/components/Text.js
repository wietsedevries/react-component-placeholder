import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ctx } from '../utils';

class Text extends Component {
  get style() {
    const { color: contextColor, borderRadius: contextBorderRadius, fontSize: contextFontSize } = this.context;
    const { color: propsColor, borderRadius: propsBorderRadius, fontSize: propsFontSize, width, rows } = this.props;
    const color = contextColor || propsColor;
    const fontSize = contextFontSize || propsFontSize;
    const borderRadius = contextBorderRadius || propsBorderRadius || 3;
    return {
      width: width && rows === 1 ? `${width}px` : '100%',
      backgroundColor: color,
      height: fontSize ? `${fontSize}px` : '16px',
      marginBottom: fontSize ? `${fontSize / 2}px` : '8px',
      borderRadius: borderRadius > (fontSize / 2) ? `${fontSize / 2}px` : `${borderRadius}px`,
    };
  }

  get rows() {
    const { rows = 1 } = this.props;
    const components = [];
    for (let i = 0; i < rows; i++) {
      const maxWidth = (i + 1 === rows && rows > 1) ? '70%' : '100%';
      components.push(
        <div name="text" key={i} style={{ ...this.style, maxWidth }} />
      );
    }
    return components;
  }

  render() {
    return (<div>{this.rows}</div>);
  }
}

Text.contextType = ctx;
Text.propTypes = {
  rows: PropTypes.number,
  width: PropTypes.number,
  color: PropTypes.string,
  fontSize: PropTypes.number,
  borderRadius: PropTypes.number,
};

export default Text;